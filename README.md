# Easy Sudoku

An easy Sudoku game written in Python.

All 132 puzzles in this game are easy difficulty and should be solvable without the need for notes.
