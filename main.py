#!/usr/bin/python
from random import randrange
import sys

def main():
    EMPTY_CELL = "." # A constant to represent an empty cell in the puzzle.

    # Create two empty lists that will both contain the same puzzle. The difference is that
    #   ORIGINAL is not supposed to be changed.
    current_puzzle = []
    ORIGINAL = []

    # This dictionary assigns a numeric value to a letter key. The letter represents a row in
    #   the puzzle given in the user's input.
    row_keys = {
        "A": 0, "B": 1, "C": 2,
        "D": 3, "E": 4, "F": 5,
        "G": 6, "H": 7, "I": 8
        }

    # Open the puzzles.dat file.
    with open("puzzles.dat") as puzzle_file:
        puzzles = puzzle_file.readlines()

    # Strip the puzzles of new line characters.
    # The enumerate() function allows the for loop to automatically index the iterations without
    #   needing to have a dedicated variable for counting the iterations. In this loop, 'i' is
    #   the index variable, and 'puzzle' is the value associated with that index.
    for i, puzzle in enumerate(puzzles):
        # Swap the value at the current index with that value stripped of the newline character.
        puzzles[i] = puzzle.strip()

    # This function gets a puzzle from the list of puzzles retrieved from the puzzles.dat file.
    def get_puzzle():
        # Get a random line of text containing a puzzle from the puzzles list.
        random_line = puzzles[randrange(len(puzzles))]
        # Split random_line into a list of 9 strings. Each string represents a row of 9 characters.
        temp_puzzle = random_line.split("|")
        # Clear both tables before adding new puzzle data.
        current_puzzle.clear()
        ORIGINAL.clear()

        # Convert each row into a list of characters and append the row list to current_puzzle
        #   and ORIGINAL.
        for row in temp_puzzle:
            current_puzzle.append(list(row))
            ORIGINAL.append(list(row))

    # Render the puzzle for the player to see.
    def render_puzzle(puzzle):
        # Print the column header.
        print("\n\033[1;33m   A B C   D E F   G H I\033[0m")

        # r is the row index.
        for r, row in enumerate(puzzle):
            # c is the cell index for each row. This also corresponds to the column. Note the
            #   row_keys dictionary above.
            for c, cell in enumerate(row):
                # If this is the first item in the row, print the row number first.
                if c == 0:
                    print("\033[1;33m" + str(r+1) + "\033[0m" + "  ", end="")
                # Print the value in the current cell. But first, check to see if the current
                #   cell is part of the original puzzle. If it is, but is not an empty cell,
                #   apply color to the output.
                if puzzle[r][c] == ORIGINAL[r][c] and puzzle[r][c] != EMPTY_CELL:
                    print("\033[1;36m" + cell + "\033[0m" + " ", end="")
                else:
                    print(cell + " ", end="")
                # Print a vertical block separator after the 3rd and 6th cell in each row.
                if c == 2 or c == 5:
                    print("| ", end="")
            # An empty print() function is needed to start a new row.
            print()

            # Print a horizontal block separator after the 3rd and 6th row.
            if r == 2 or r == 5:
                print("   ------+-------+-------")

    # This function will change the value of the specified cell to the user-provided number.
    def update_cell(column, row, number):
        # Check to make sure the cell being changed does not contain an original puzzle value. Does the
        #   cell in the original puzzle contain a '.' character (the value of EMPTY_CELL).
        if ORIGINAL[int(row)-1][row_keys[column]] == EMPTY_CELL:
            # Change the specified cell to the value of number.
            if number == "0":
                current_puzzle[int(row)-1][row_keys[column]] = EMPTY_CELL
            else:
                current_puzzle[int(row)-1][row_keys[column]] = number
        else:
            print(f"\n\033[1;31m Cell {column}{row} cannot be changed.\033[0m")

    # This function will reset the current puzzle to its original state.
    def reset_puzzle():
        print("\n\033[1;37m Resetting puzzle...\033[0m")
        # Replace all values of current_puzzle with the values of ORIGINAL.
        for r, row in enumerate(current_puzzle):
            for c, cell in enumerate(row):
                if ORIGINAL[r][c] == EMPTY_CELL:
                    current_puzzle[r][c] = ORIGINAL[r][c]
                else:
                    current_puzzle[r][c] = ORIGINAL[r][c]

    # This function will save the current puzzle and the original puzzle to the save.dat file.
    def save_puzzle():
        print("\n\033[1;37m Puzzle saved.\033[0m")
        # Create two empty strings for storing the puzzle strings to write to the data file.
        saved_puzzle = ""
        saved_original = ""

        # Reconstruct the full puzzle strings to be written to the save file.
        # Build the string for progress made on the current puzzle.
        for r, row in enumerate(current_puzzle):
            for c, cell in enumerate(row):
                saved_puzzle += current_puzzle[r][c]
                if r != 8 and c == 8:
                    saved_puzzle += "|"
        # Build the string for the original puzzle.
        for r, row in enumerate(ORIGINAL):
            for c, cell in enumerate(row):
                saved_original += ORIGINAL[r][c]
                if r != 8 and c == 8:
                    saved_original += "|"

        # Write the strings to the save file, replacing any data already in the file.
        with open("save.dat", "w") as save_file:
            save_file.write(saved_puzzle + "\n")
            save_file.write(saved_original)

    # This function will read the save.dat file and store each line into the respective puzzle
    #   list variable. The first line of the file is the puzzle currently being worked, and the
    #   second line is the original puzzle.
    def load_puzzle():
        # Open the save.dat file and add each line to a list.
        print("\n\033[1;37m Loading puzzle...\033[0m")
        with open("save.dat") as save_file:
            saves = save_file.readlines()

        # Strip the new line characters from the lines of the save file.
        for i, save in enumerate(saves):
            saves[i] = save.strip()

        # Clear both tables before adding new puzzle data.
        current_puzzle.clear()
        ORIGINAL.clear()

        # Split the puzzles into rows of strings.
        puzzle_save = saves[0].split("|")
        original_save = saves[1].split("|")

        # Convert each row of the loaded puzzles into a list of characters and append the row
        #   lists to their respective puzzle variable.
        for row in puzzle_save:
            current_puzzle.append(list(row))
        for row in original_save:
            ORIGINAL.append(list(row))

    # This function will check the puzzle for empty cells and errors in the rows and columns
    #   of the puzzle. If there are no empty cells, no duplicates in each row, and no duplicates
    #   in each column, the player has solved the puzzle.
    def check_puzzle(puzzle):
        # Booleans for tracking the solved state of the puzzle.
        empty_cells = False
        row_dup = False
        col_dup = False

        # This table will contain the columns of characters. If this list is created as an
        #   empty list, it throws a "list index out of range" error. The error goes away if
        #   the list is created as 9 rows of 9 empty strings.
        # A better way of doing this may need to be explored at a later time.
        columns = [
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ]

        # Build the columns table.
        for r, row in enumerate(puzzle):
            for c, cell in enumerate(row):
                columns[c][r] = puzzle[r][c]

        # Check for empty cells.
        for r, row in enumerate(puzzle):
            if EMPTY_CELL in row:
                print(f"\n\033[1;31m The puzzle contains an empty cell in row {r+1}.\033[0m")
                empty_cells = True
                break

        # If there are no empty cells, check for duplicate numbers in the puzzle rows.
        if empty_cells == False:
            for r, row in enumerate(puzzle):
                if len(row) != len(set(row)):
                    print(f"\n\033[1;31m Row {r+1} contains a duplicate.\033[0m")
                    row_dup = True
                    break

        # If there are no empty cells and no duplicates in the rows, check for duplicate
        #   numbers in the puzzle columns.
        if empty_cells == False and row_dup == False:
            for c, col in enumerate(columns):
                if len(col) != len(set(col)):
                    print(f"\n\033[1;31m Column {c+1} contains a duplicate.\033[0m")
                    col_dup = True
                    break

        # Final checks.
        if empty_cells == False and row_dup == False and col_dup == False:
            print("\n\033[1;32m You solved the puzzle!\033[0m")

    # This function simply displays the options to the player and explains what each option does.
    def show_help():
        print()
        print("\033[1;37m OPTIONS:\033[0m")
        print("-"*75)
        print("   HELP, H  |  Show this list of options.")
        print("   CHECK, C |  Check the puzzle for errors or if it has been solved.")
        print("   NEW, N   |  Load a new puzzle.")
        print("   RESET, R |  Reset the current puzzle to its original state.")
        print("   SAVE, S  |  Save the current state of the puzzle to the save.dat file.")
        print("   LOAD, L  |  Load the puzzle currently stored in the save.dat file.")
        print("   QUIT, Q  |  Quit the game. Unsaved progress is lost.")
        print("-"*75)

    # This function simply exits the script.
    def quit():
        print("\n\033[1;37m Quitting...\033[0m\n")
        sys.exit(0)

    # Get a puzzle before entering the main loop.
    get_puzzle()

    # Main game loop.
    while True:
        # Display the current puzzle.
        render_puzzle(current_puzzle)

        print("\nEnter a number, or HELP, CHECK, NEW, RESET, SAVE, LOAD, QUIT.")
        print("To enter a number, specify the column and row, and then the number.")
        print("Example: C7 3 (i.e., place a 3 in column C, row 7)")
        command = input("> ").upper().strip()

        # Menu logic.
        #if len(command) > 0 and command[0] in ("H", "C", "N", "R", "S", "L", "Q"):
        if command == "HELP" or command == "H":
            show_help()
        elif command == "CHECK" or command == "C":
            check_puzzle(current_puzzle)
        elif command == "NEW" or command == "N":
            print("\n\033[1;37m Getting new puzzle...\033[0m")
            get_puzzle()
        elif command == "RESET" or command == "R":
            reset_puzzle()
        elif command == "SAVE" or command == "S":
            save_puzzle()
        elif command == "LOAD" or command == "L":
            load_puzzle()
        elif command == "QUIT" or command == "Q":
            quit()
        else:
            # Anything else is either a change to the puzzle or invalid.
            # If a space is in the command, then check to see if it is a valid change to the puzzle.
            if " " in command:
                # After the split, there should only be two items in the list.
                if len(command.split()) == 2:
                    # Split the list into the two parts.
                    cell, number = command.split()
                    # A cell designation should only be two characters in length.
                    if len(cell) != 2:
                        print(f"\n\033[1;31m Cell {cell} not recognized.\033[0m")
                        continue
                    # The number to put in the cell should be a number between 0 and 9. The number will
                    #   not be converted to an integer because it needs to remain as a string to go
                    #   into the row string. If the number is 0, this wil revert the cell to empty.
                    if not number.isdecimal() or not (0 <= int(number) <= 9):
                        print(f"\n\033[1;31m Please enter a number between 0 and 9. You entered {number}.\033[0m")
                        continue
                    # The two characters in the cell designation should consist of first a letter found
                    #   in the row_keys dictionary and then a number between 1 and 9.
                    if cell[0] in row_keys.keys() and (1 <= int(cell[1]) <= 9):
                        # If all input is valid, update the cell.
                        update_cell(cell[0], cell[1], number)
                    else:
                        print(f"\n\033[1;31m Invalid cell input: {cell}\033[0m")
            else:
                print("\n\033[1;31m Invalid input.\033[0m")

if __name__ == '__main__':
    main()
